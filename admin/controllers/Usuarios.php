<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if($this->session->userdata('logged_in')){
			$data['configuracao'] = array(
				"titulo" => "Usuários",
				"descricao" => "Gerenciamento de usuários",
				"icone" => "fa fa-users"
			);

			$this->load->view('layout/header',$data);
			$this->load->view('usuarios/index');
			$this->load->view('layout/footer');

		}else{
			redirect('login', 'refresh');
		}   
	}
  
}

?>