<nav class="navbar navbar-static-top">

  <div class="navbar-custom-menu">
	<ul class="nav navbar-nav">

	  <!-- Notifications: style can be found in dropdown.less -->
	  <li class="dropdown notifications-menu">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
		  <i class="fa fa-bell-o"></i>
		  <span class="label label-warning">1</span>
		</a>
		<ul class="dropdown-menu">
		  <li class="header">Você possui 1 notificação</li>
		  <li>
			<!-- inner menu: contains the actual data -->
			<ul class="menu">
			  <li>
				<a href="#">
				  <i class="fa fa-users text-aqua"></i> 5 novos membros
				</a>
			  </li>
			</ul>
		  </li>
		  <li class="footer"><a href="#">Ver todas</a></li>
		</ul>
	  </li>

	  <li class="dropdown user user-menu">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
		  <img src="<?php echo base_url('public/admin/img/upload')."/".$this->session->userdata('img_perfil');?>" class="user-image" alt="<?php echo $this->session->userdata('nome');?>">
		  <span class="hidden-xs"><?php echo $this->session->userdata('nome');?></span>
		</a>
		<ul class="dropdown-menu">
		  <li class="user-header">
			<img src="<?php echo base_url('public/admin/img/upload')."/".$this->session->userdata('img_perfil');?>" class="img-circle" alt="<?php echo $this->session->userdata('nome');?>">

			<p>
			  <small>Membro desde <?php echo $this->session->userdata('data_cadastro');?></small>
			</p>
		  </li>

		  <li class="user-footer">
			<div class="pull-left">
			  <a href="#" class="btn btn-default btn-flat">Perfil</a>
			</div>
			<div class="pull-right">
			  <a href="<?php echo base_url('login/logout');?>" class="btn btn-default btn-flat">Logout</a>
			</div>
		  </li>
		</ul>
	  </li>
	</ul>
  </div>
</nav>