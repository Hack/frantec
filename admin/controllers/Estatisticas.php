<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Estatisticas extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if($this->session->userdata('logged_in')){
			$data['configuracao'] = array(
				"titulo" => "Estatísticas",
				"descricao" => "Gráficos e relatórios",
				"icone" => "fa fa-bar-chart"
			);

			$this->load->view('layout/header',$data);
			$this->load->view('estatisticas/index');
			$this->load->view('layout/footer');

		}else{
			redirect('login', 'refresh');
		}   
	}
  
}

?>