<aside class="main-sidebar">
	<section class="sidebar">
	  <div class="user-panel">
		<div class="pull-left image">
		  <img src="<?php echo base_url('public/admin/img/upload')."/".$this->session->userdata('img_perfil');?>" class="img-circle" alt="<?php echo $this->session->userdata('nome');?>">
		</div>
		<div class="pull-left info">
		  <p><?php echo $this->session->userdata('nome');?></p>
		  <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
		</div>
	  </div>

	  <ul class="sidebar-menu">
		<li class="header">MENU</li>

		<?php
			if($this->uri->segment(1) == 'dashboard'){
				$class = "class='active'";
			}else{
				$class="";
			}
		?>

		<li <?php echo $class;?>>
	  		<a href="<?php echo base_url('dashboard');?>">
				<i class="fa fa-dashboard"></i> 
				<span>Dashboard</span>
		  	</a>
		</li>

		<?php
			$menu = $this->Menu_model->CarregarMenu();
			if(!empty($menu)){
				foreach($menu as $value){
					if($value['controller'] == $this->uri->segment(1)){
						$class = "class='active'";
					}else{
						$class="";
					}					
		?>
					<li class="mt">
						<a href="<?php echo base_url("{$value['controller']}/");?>" <?php echo $class;?>>
							<i class="<?php echo $value['icone'];?>"></i>
							<span><?php echo $value['descricao'];?></span>
						</a>
					</li>
		<?php
				}
			}
		?>		

	  </ul>
	</section>
</aside>