<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comprovantes extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if($this->session->userdata('logged_in')){
			$data['configuracao'] = array(
				"titulo" => "Comprovantes",
				"descricao" => "Medindo seu sucesso",
				"icone" => "fa fa-file-text-o"
			);

			$this->load->view('layout/header',$data);
			$this->load->view('comprovantes/index');
			$this->load->view('layout/footer');

		}else{
			redirect('login', 'refresh');
		}   
	}
  
}

?>